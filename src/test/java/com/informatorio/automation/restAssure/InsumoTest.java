package com.informatorio.automation.restAssure;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.informatorio.automation.model.Supply;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class InsumoTest {

	private Supply supply = new Supply(3, "Post-it", 100);
	
	@BeforeMethod
	public void setUp() {
		
		RestAssured.baseURI = "http://localhost";
		RestAssured.port = 3000;
		RestAssured.basePath = "/api/insumo";
		
	}
	
	@Test
	public void getAllSupplies() {
		
		Response response = RestAssured.get();
		
		assertEquals(response.statusCode(), 200);
		
		JsonPath json = response.jsonPath();
		
		assertEquals(json.getInt("insumos[1].id"), 2);
		assertEquals(json.getInt("insumos[1].cantidad"), 5);
		assertEquals(json.getString("insumos[1].nombre"), "papel A4");
		
	}
	
	@Test 
	public void getByIdUsingPathParam() {
		
		Response response = RestAssured.given()
										.pathParam("insumoId", "3")
										.get("/{insumoId}");
		
		assertEquals(response.getStatusCode(), 200);
		
		JsonPath json = response.jsonPath();
		
		assertEquals(json.getString("mensaje"), "Insumo no encontrado");
		
	}
	
	@Test(dependsOnMethods = "getByIdUsingPathParam")
	public void addSupply() {
		
		Response response = RestAssured.given()
										.contentType(ContentType.JSON)
										.body(supply)
										.post();
		
		assertEquals(response.statusCode(), 201);
		
		JsonPath json = response.jsonPath();

		assertEquals(json.getString("mensaje"), "El insumo se ha agregado");
	
	}
	
	@Test(dependsOnMethods = "addSupply")
	public void getByIdUsingQueryParam() {
		
		Response response = RestAssured.given()
										.queryParam("id", supply.getId())
										.get();
		
		assertEquals(response.statusCode(), 200);
		
		JsonPath json = response.jsonPath();

		assertEquals(json.getInt("id"), supply.getId());
		assertEquals(json.getString("name"), supply.getName());
		assertEquals(json.getInt("amount"), supply.getAmount());
		
	}
	
	@Test(dependsOnMethods = "getByIdUsingQueryParam")
	public void deleteProduct(){
		
		Response response = RestAssured.given()
										.pathParam("id", supply.getId())
										.delete("/{id}");
		
		assertEquals(response.statusCode(), 200);
		
		JsonPath json = response.jsonPath();
		
		assertEquals(json.getString("message"), "El insumo ha sido eliminado exitosamente.");
	
	}
	
}
